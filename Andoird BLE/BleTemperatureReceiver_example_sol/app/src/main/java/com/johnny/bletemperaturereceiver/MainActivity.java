package com.johnny.bletemperaturereceiver;

// Pääactivityn näkymän toiminnot
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Set;
import java.util.ArrayList;
import android.widget.Toast;
import android.widget.ArrayAdapter;
import android.widget.AdapterView
import android.widget.AdapterView.OnClickListener
import android.widget.TextView;
import android.content.Intent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
		//muuttujien alustuksia
public class MainActivity extends AppCompatActivity {
	private static final String TAG = MainActivity.class.getSimpleName();

	private static final int REQUEST_SELECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;

	public static class BleConnectionStatus {
		private static final int CONNECTED = 20;
		private static final int DISCONNECTED = 21;
	}

	private int mState = BleConnectionStatus.DISCONNECTED;
	private BluetoothDevice mDevice = null;
	private BluetoothAdapter mBtAdapter;
	private TextView labelDeviceName;
	private TextView labelTemperature;
	private TextView labelHumidity;
	private Button buttonConnect;

	private ButtonON buttonOn;
	private ButtonOFF buttonOff;

	private BLETemperatureService mService = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mBtAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBtAdapter == null) {
			Toast.makeText(this, R.string.bluetooth_is_not_available, Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		//Activityn näkymät
		labelTemperature = (TextView) findViewById(R.id.label_temperature);
		labelHumidity = (TextView) findViewById(R.id.label_humidity);
		labelDeviceName = (TextView) findViewById(R.id.label_device_name);

		buttonConnect = (Button) findViewById(R.id.btn_connect);
		buttonOn = (ButtonON) findViewById(R.id.btn_ledon);
		buttonOff = (ButtonOFF) findViewById(R.id.btn_ledoff);

		buttonConnect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (maybeRequestPermission()) {
					return;
				}

				if (!mBtAdapter.isEnabled()) {
					Log.i(TAG, "onClick - BT not enabled yet");
					Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
				} else {
					if (mState == BleConnectionStatus.DISCONNECTED) {

						//Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices
						intentSearchDevices();
					} else {
						//Disconnect button pressed
						if (mDevice != null) {
							mService.disconnect();
						}
					}
				}
			}
		});

		buttonConnect.setEnabled(false);
		serviceInit();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(uartStatusChangeReceiver);
		} catch (Exception ignore) {
			Log.e(TAG, ignore.toString());
		}
		unbindService(mServiceConnection);
		mService = null;
	}
	//Tarkastetaan toimintoja
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {

			case REQUEST_SELECT_DEVICE:
				//When the DeviceListActivity return, with the selected device address
				if (resultCode == Activity.RESULT_OK && data != null) {
					String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
					mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);

					String deviceName = "";
					if (mDevice != null) {
						Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
						deviceName = mDevice.getName();
					}

					labelDeviceName.setText(String.format(getString(R.string.device_connecting), deviceName));
					if (mService != null) {
						mService.connect(deviceAddress);
					}

				}
				break;
			case REQUEST_ENABLE_BT:
				// When the request to enable Bluetooth returns
				if (resultCode == Activity.RESULT_OK) {
					Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();
				} else {
					// User did not enable Bluetooth or an error occurred
					Log.d(TAG, "BT not enabled");
					Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
					finish();
				}
				break;
			default:
				Log.e(TAG, "wrong request code");
				break;
		}
	}
	// Otetaan yhteys BLE laitteeseen
	private final ServiceConnection mServiceConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder rawBinder) {
			mService = ((BLETemperatureService.LocalBinder) rawBinder).getService();
			if (!mService.initialize()) {
				Log.e(TAG, "Unable to initialize Bluetooth");
				finish();
			}
			if (mService.getConnectionState() == BLETemperatureService.ConnectState.Connected) {
				if (mDevice == null) {
					mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(mService.getBluetoothDeviceAddress());
				}
				mState = BleConnectionStatus.CONNECTED;
			} else {
				mState = BleConnectionStatus.DISCONNECTED;
			}
			updateConnectionState();

			buttonConnect.setEnabled(true);
		}

		public void onServiceDisconnected(ComponentName classname) {
			mService = null;
		}
	};
			//BLE toimintoja
	private final BroadcastReceiver uartStatusChangeReceiver = new BroadcastReceiver() {

		public void onReceive(Context context, final Intent intent) {
			String action = intent.getAction();
			if (action.equals(BLETemperatureService.ACTION_GATT_CONNECTED)) {
				runOnUiThread(new Runnable() {
					public void run() {
						mState = BleConnectionStatus.CONNECTED;
						updateConnectionState();
					}
				});
			} else if (action.equals(BLETemperatureService.ACTION_GATT_DISCONNECTED)) {
				runOnUiThread(new Runnable() {
					public void run() {
						mState = BleConnectionStatus.DISCONNECTED;
						updateConnectionState();
						if (mService != null) {
							mService.close();
						}
					}
				});
			} else if (action.equals(BLETemperatureService.NOT_SUPPORT_TEMPERATURE_SERVICE)) {
				Toast.makeText(MainActivity.this, R.string.temperature_service_not_found, Toast.LENGTH_SHORT).show();
			} else if (action.equals(BLETemperatureService.ACTION_TEMPERATURERE_UPDATE)) {
				runOnUiThread(new Runnable() {


					public void run() {
						double temperature = intent.getDoubleExtra(BLETemperatureService.EXTRA_TEMPERATURERE_DATA, 0);
						//float temperature = intent.getFloatExtra(BLETemperatureService.EXTRA_TEMPERATURERE_DATA, 0);
						labelTemperature.setText(String.format(getString(R.string.temperature_template), temperature));
						//labelTemperature.setText(String.format(getString(R.string.temperature_template), temperature));
						if (temperature > 30 || temperature < 0)
						{
							Toast.makeText(getApplicationContext(), "Temperature Alarm", Toast.LENGTH_SHORT).show();
						}
						//FIREBASE alustus
						Firebase - Basic syntax

						private DatabaseReference mDatabase;
						private DatabaseReference mMessageReference;
						// ...
						mDatabase = FirebaseDatabase.getInstance().getReference();
						mMessageReference = FirebaseDatabase.getInstance().getReference("message");*/

						FirebaseDatabase database = FirebaseDatabase.getInstance();
						DatabaseReference myRef = database.getReference("Temperature");

						setValue() to:
						– save data to a specified reference (myref)
						– replace any existing data at that reference*/

						myRef.setValue(temperature);
					}
				});
			}
					else if (action.equals(BLETemperatureService.ACTION_HUMIDITY_UPDATE)) {
					runOnUiThread(new Runnable() {
						public void run() {
						double humidity = intent.getDoubleExtra(BLETemperatureService.EXTRA_HUMIDITY_DATA, 0);
								labelHumidity.setText(String.format(getString(R.string.humidity_template), humidity));
								if (humidity > 60)
								{
									Toast.makeText(getApplicationContext(), "Moisture Alarm", Toast.LENGTH_SHORT).show();
								}
							FirebaseDatabase database = FirebaseDatabase.getInstance();
							DatabaseReference myRef = database.getReference("Humidity");
							myRef.setValue(humidity);
					}
				});
			}

		}
	};

	private void intentSearchDevices() {
		Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
		startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
	}

	private void updateConnectionState() {
		if (mState == BleConnectionStatus.CONNECTED) {
			buttonConnect.setText(R.string.disconnect);
			String deviceName = "";
			if (mDevice != null) {
				deviceName = mDevice.getName();
			}
			labelDeviceName.setText(String.format(getString(R.string.device_connected), deviceName));
		} else {
			buttonConnect.setText(R.string.connect);
			labelDeviceName.setText(R.string.not_connected);
			labelTemperature.setText(R.string.temperature_unknown);
			labelHumidity.setText(R.string.humidity_unknown);
		}
	}

	private void serviceInit() {
		Intent bindIntent = new Intent(this, BLETemperatureService.class);
		bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

		LocalBroadcastManager.getInstance(this).registerReceiver(uartStatusChangeReceiver, makeGattUpdateIntentFilter());
	}

	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BLETemperatureService.ACTION_GATT_CONNECTED);
		intentFilter.addAction(BLETemperatureService.ACTION_GATT_DISCONNECTED);
		intentFilter.addAction(BLETemperatureService.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(BLETemperatureService.ACTION_TEMPERATURERE_UPDATE);
		intentFilter.addAction(BLETemperatureService.ACTION_HUMIDITY_UPDATE);
		intentFilter.addAction(BLETemperatureService.NOT_SUPPORT_TEMPERATURE_SERVICE);
		return intentFilter;
	}

	// Permission request listener method

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
	                                       @NonNull int[] grantResults) {
		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			intentSearchDevices();
		} else {
			Toast.makeText(getApplicationContext(), R.string.permission_denied,
					Toast.LENGTH_LONG).show();
			finish();
		}
	}

	// Permission management methods

	/**
	 * Checks whether it is necessary to ask for permission to access coarse location. If necessary, it also
	 * requests permission.
	 *
	 * @return true if a permission request is made. False if it is not necessary.
	 */
	@TargetApi(23)
	private boolean maybeRequestPermission() {
		if (requiresPermission()) {
			requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
			return true;
		} else {
			return false;
		}
	}

	@TargetApi(23)
	private boolean requiresPermission() {
		return Build.VERSION.SDK_INT >= 23
				&& checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED;
	}
}

//Led käskyn lähettäminen
//yhteyden aloittaminen

private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
{
	private boolean ConnectSuccess = true; //if it's here, it's almost connected

	@Override
	protected void onPreExecute()
	{
		progress = ProgressDialog.show(ledControl.this, "Connecting...", "Please wait!!!");  //show a progress dialog
	}

	@Override
	protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
	{
		try
		{
			if (btSocket == null || !isBtConnected)
			{
				myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
				BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
				btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
				BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
				btSocket.connect();//start connection
			}
		}
		catch (IOException e)
		{
			ConnectSuccess = false;//if the try failed, you can check the exception here
		}
		return null;
	}
	@Override
	protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
	{
		super.onPostExecute(result);

		if (!ConnectSuccess)
		{
			msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
			finish();
		}
		else
		{
			msg("Connected.");
			isBtConnected = true;
		}
		progress.dismiss();
	}
}

// kuunnellaan buttonien painamista

btnOn.setOnClickListener(new View.OnClickListener()
		{
@Override
public void onClick(View v)
		{
		turnOnLed();      //method to turn on
		}
		});

		btnOff.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v)
		{
		turnOffLed();   //method to turn off
		}
		});

		btnDis.setOnClickListener(new View.OnClickListener()
		{
@Override
public void onClick(View v)
		{
		Disconnect(); //close connection
		}
		});

		brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
@Override
public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (fromUser==true)
		{
		lumn.setText(String.valueOf(progress));
		try
		{
		btSocket.getOutputStream().write(String.valueOf(progress).getBytes());
		}
		catch (IOException e)
		{

		}
		}
		}

@Override
public void onStartTrackingTouch(SeekBar seekBar) {

		}

@Override
public void onStopTrackingTouch(SeekBar seekBar) {

		}
		});

// error messageja ja toasteja

private void msg(String s)
		{
		Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
		}
private void Disconnect()
		{
		if (btSocket!=null) //If the btSocket is busy
		{
		try
		{
		btSocket.close(); //close connection
		}
		catch (IOException e)
		{ msg("Error");}
		}
		finish(); //return to the first layout
		}
private void turnOffLed()
		{
		if (btSocket!=null)
		{
		try
		{
		btSocket.getOutputStream().write("TF".toString().getBytes());
		}
		catch (IOException e)
		{
		msg("Error");
		}
		}
		}
private void turnOnLed()
		{
		if (btSocket!=null)
		{
		try
		{
		btSocket.getOutputStream().write("TO".toString().getBytes());
		}
		catch (IOException e)
		{
		msg("Error");
		}
		}
		}
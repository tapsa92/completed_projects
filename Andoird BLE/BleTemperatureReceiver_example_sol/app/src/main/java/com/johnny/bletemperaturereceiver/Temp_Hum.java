package com.johnny.bletemperaturereceiver;

/**
 * Created by Eero on 31.10.2018.
 */
// säilötään Arduino koodiin liittyviä globaaleja muuttujia

public class Temp_Hum  {

private double Temperature;
private double Humidity;

public double getTemperature() {
    return Temperature;
}

 public void setTemperature(double temperature) {
    this.Temperature = temperature;
}

public double getHumidity() {
    return Humidity;
}

public void setHumidity(double humidity) {
        this.Humidity = humidity;
    }

}

# Komennot

## Joko

git clone - ei toimi

## Tai

git init - tehdään local repo

git remote add origin - linkkaus olemassa olevaan remote reposityyn

git pull origin master - vedetään viimeisimmät tiedot remotesta localiin

## Elämää helpottavaa

git status - muutoksia local repo kansiossa

git add . - lisää kaikki localisti kansioon lisätyt tiedostot committiin

git commit - "vittumainen editori" git commit -m "" - tekeee saman mutta yhdellä pikaotsikolla

git push origin master -lisää kaikki committiin lisätyt tiedostot remote repoon

## Branchin tekeminen ja merge pyyntö

git checkout -b branchinnimi - vaihdetaan uuteen branchiin johon sitten muutoksen siirretään

git push -o merge_request.create origin branchinnimi - lähetetään branch rouva fuhrermeisterille joka luojan lykyllä hyväksytyttää työn
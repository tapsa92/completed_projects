/*
  ADXL335B Accelerometer in analog input
  Output to serial port
*/

/*
Sensor connected in channels:
X-axis A0 (kortin tekstien suuntaisesti)
Y-axis A1 (poikittain kortin teksteihin nähden)
Z-axis A2 (kortin pystysuunnassa)

Sensor VCC +3.3 V --> 330 mV/g
0 g = VCC/2 -> 1,65 V
- 3 g = 1,65 V - 3 * 330 mV = 0,66 V
+ 3 g = 1,65 V + 3 * 330 mV = 2,64 V
*/

// These constants won't change. They're used to give names to the pins used:
const int analogInPin_0 = A0;  // Analog input pin that the X-axis is attached to
const int analogInPin_1 = A1;  // Analog input pin that the Y-axis is attached to
const int analogInPin_2 = A2;  // Analog input pin that the Z-axis is attached to

//calibrate sensor by using these values
//if 1 g position (test using earth gravity) is shifted, add or subtract same amount of minimum and maximum values depending of offset error direction
const int minimumX = 125.000;
const int maximumX = 530.000;
const int minimumY = 125.000;
const int maximumY = 530.000;
const int minimumZ = 135.000;
const int maximumZ = 540.000;

long int sensorValue_X = 0;        // value read from the X-axis
long int sensorValue_Y = 0;        // value read from the Y-axis
long int sensorValue_Z = 0;        // value read from the Z-axis

//to hold the caculated angle values NOT IN USE
//double x;
//double y;
//double z;

double MaxTotalAcc = 0;

//define fmap-function to scale float-type variables
float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
}

void loop() {
  
  // read the analog in value:
  sensorValue_X = analogRead(analogInPin_0);
  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);
  
  sensorValue_Y = analogRead(analogInPin_1);
  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);
  
  sensorValue_Z = analogRead(analogInPin_2);
  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);

//convert read values to degrees -90 to 90 – Needed for atan2
// int xAng = map(sensorValue_X, minimumX, maximumX, -90, 90);
// int yAng = map(sensorValue_Y, minimumY, maximumY, -90, 90);
// int zAng = map(sensorValue_Z, minimumZ, maximumZ, -90, 90);


//Caculate 360deg values like so: atan2(-yAng, -zAng)
//atan2 outputs the value of -π to π (radians)
//We are then converting the radians to degrees
// x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
// y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
// z = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);


 //Serial.println("X-axis raw = ");
 //Serial.println(sensorValue_X); //raw X-axis
 double sensorValue_X_g = fmap(sensorValue_X, minimumX, maximumX, -3.000, 3.000);
 //double sensorValue_X_g = fmap(sensorValue_X, 0, 1023, -5.000, 10.000);
 Serial.print(" X: ");
 Serial.print(sensorValue_X_g); //Acceleration X-axis
 //Serial.print(" ");
 //Serial.print(x);
 //Serial.print(" deg");
 delay(2);

 //Serial.println("Y-axis raw = ");
 //Serial.println(sensorValue_Y); //raw X-axis
 double sensorValue_Y_g = fmap(sensorValue_Y, minimumY, maximumY, -3.000, 3.000);
 //double sensorValue_Y_g = fmap(sensorValue_Y, 0, 1023, -5.000, 10.000);
 Serial.print(" Y: ");
 Serial.print(sensorValue_Y_g); //Acceleration Y-axis
 //Serial.print(" ");
 //Serial.print(y);
 //Serial.print(" deg");
 delay(2);
 
 //Serial.println("Z-axis raw = ");
 //Serial.println(sensorValue_Z); //raw X-axis
 double sensorValue_Z_g = fmap(sensorValue_Z, minimumZ, maximumZ, -3.000, 3.000);
 //double sensorValue_Z_g = fmap(sensorValue_Z, 0, 1023, -5.000, 10.000);
 Serial.print(" Z: ");
 Serial.print(sensorValue_Z_g); //Acceleration Z-axis
 //Serial.print(" ");
 //Serial.print(z);
 //Serial.print(" deg");
 delay(2);

/*
If you hang the card from the usb-cable, then x-axis senses the gravity 
and y and z -axis senses the acceleration caused by force of hitting the bag.
So, only y and z-axis values can be used when calculating total acceleration.
//Maximum combined X/Y-axis acceleration
MaxTotalAcc = sqrt(sq(sensorValue_Y_g) + sq(sensorValue_Z_g));
*/

/*
If accelerometer position and orientation is not known, then all axis can be used
*/
//Maximum acceleration all 3 axes combined
 MaxTotalAcc = sqrt(sq(sensorValue_X_g) + sq(sensorValue_Y_g) + sq(sensorValue_Z_g));
 Serial.print(" Max: ");
 Serial.print(MaxTotalAcc);
 Serial.print(" g");
 Serial.println();
 //endof_scaling

}

/************************************************************
kajaanin Ammattikorkeakoulu
Projektiopinnot_2
Arduino Uno

Lyönnistä mitataan voima kiihtyvyysanturn ja nyrkkeilysäkin avulla

Aleksi Saukko, Niilo Niskann, Valtteri Lampinen, Jari Korhonen, Tapio Ohtonen
*************************************************************/
#include <MPU9250_RegisterMap.h>
#include <SparkFunMPU9250-DMP.h>

/************************************************************
MPU9250_Basic
 Basic example sketch for MPU-9250 DMP Arduino Library 
Jim Lindblom @ SparkFun Electronics
original creation date: November 23, 2016
https://github.com/sparkfun/SparkFun_MPU9250_DMP_Arduino_Library

This example sketch demonstrates how to initialize the 
MPU-9250, and stream its sensor outputs to a serial monitor.

Development environment specifics:
Arduino IDE 1.6.12
SparkFun 9DoF Razor IMU M0

Supported Platforms:
- ATSAMD21 (Arduino Zero, SparkFun SAMD21 Breakouts)
*************************************************************/
#include <SparkFunMPU9250-DMP.h>
#include <SoftwareSerial.h>
#include <Wire.h>

/*************************************************************/
//Code to set up bluetooth transmission
SoftwareSerial BTserial(10, 11); // RX | TX Ports for communication

int sensorPin = A0; //Reads ADC value from port A0
int sensorValue = 0; 

/*************************************************************/
float totalNewtons;
float totalACC;

MPU9250_DMP imu;

void setup() 
{
  BTserial.begin(9600); // Baud rate for bluetooth serial communication
  
  Serial.begin(115200); //Baud rate for accelerometer

  // Call imu.begin() to verify communication with and
  // initialize the MPU-9250 to it's default values.
  // Most functions return an error code - INV_SUCCESS (0)
  // indicates the IMU was present and successfully set up
  if (imu.begin() != INV_SUCCESS)
  {
    while (1)
    {
      Serial.println("Unable to communicate with MPU-9250"); //Print error message if necessary
      Serial.println("Check connections, and try again.");
      Serial.println();
      delay(5000);
    }
  }

  // Use setSensors to turn on or off MPU-9250 sensors.
  // Any of the following defines can be combined:
  // INV_XYZ_GYRO, INV_XYZ_ACCEL, INV_XYZ_COMPASS,
  // INV_X_GYRO, INV_Y_GYRO, or INV_Z_GYRO
  // Enable all sensors:
  imu.setSensors(INV_XYZ_GYRO | INV_XYZ_ACCEL | INV_XYZ_COMPASS);

  // Use setGyroFSR() and setAccelFSR() to configure the
  // gyroscope and accelerometer full scale ranges.
  // Gyro options are +/- 250, 500, 1000, or 2000 dps
  imu.setGyroFSR(2000); // Set gyro to 2000 dps
  // Accel options are +/- 2, 4, 8, or 16 g
  imu.setAccelFSR(16); // Set accel to +/-2g
  // Note: the MPU-9250's magnetometer FSR is set at 
  // +/- 4912 uT (micro-tesla's)

  // setLPF() can be used to set the digital low-pass filter
  // of the accelerometer and gyroscope.
  // Can be any of the following: 188, 98, 42, 20, 10, 5
  // (values are in Hz).
  imu.setLPF(5); // Set LPF corner frequency to 5Hz

  // The sample rate of the accel/gyro can be set using
  // setSampleRate. Acceptable values range from 4Hz to 1kHz
  imu.setSampleRate(10); // Set sample rate to 10Hz

  // Likewise, the compass (magnetometer) sample rate can be
  // set using the setCompassSampleRate() function.
  // This value can range between: 1-100Hz
  imu.setCompassSampleRate(10); // Set mag rate to 10Hz
}

void loop() 
{
  // dataReady() checks to see if new accel/gyro data
  // is available. It will return a boolean true or false
  // (New magnetometer data cannot be checked, as the library
  //  runs that sensor in single-conversion mode.)
  if ( imu.dataReady() )
  {
    // Call update() to update the imu objects sensor data.
    // You can specify which sensors to update by combining
    // UPDATE_ACCEL, UPDATE_GYRO, UPDATE_COMPASS, and/or
    // UPDATE_TEMPERATURE.
    // (The update function defaults to accel, gyro, compass,
    //  so you don't have to specify these values.)
    imu.update(UPDATE_ACCEL | UPDATE_GYRO | UPDATE_COMPASS);
    printIMUData();
    //BluetoothSendData();
  }
}

void printIMUData(void)
{  
  // After calling update() the ax, ay, az, gx, gy, gz, mx,
  // my, mz, time, and/or temerature class variables are all
  // updated. Access them by placing the object. in front:

  // Use the calcAccel, calcGyro, and calcMag functions to
  // convert the raw sensor readings (signed 16-bit values)
  // to their respective units.
  float accelX = imu.calcAccel(imu.ax);
  float accelY = imu.calcAccel(imu.ay);
  float accelZ = imu.calcAccel(imu.az);
  float gyroX = imu.calcGyro(imu.gx);
  float gyroY = imu.calcGyro(imu.gy);
  float gyroZ = imu.calcGyro(imu.gz);
  float magX = imu.calcMag(imu.mx);
  float magY = imu.calcMag(imu.my);
  float magZ = imu.calcMag(imu.mz);
  float totalACC = sqrt(sq(accelX*9.81)+sq(accelY*9.81))-1.20;
  float totalNewtons = totalACC*17.7;
  
  //Serial.print("Accel: " + String(accelX) + ", " +
              //String(accelY) + ", " + " g ");
  Serial.print(totalNewtons);
  //Serial.println(totalACC-9.81);
  //Serial.print(" Gyro: " + String(gyroX) + ", " +
              //String(gyroY) + ", " + String(gyroZ) + " dps");
 // Serial.print("Mag: " + String(magX) + ", " +
           //  String(magY) + ", " + String(magZ) + " uT");
  //Serial.println("Time: " + String(imu.time) + " ms");
  Serial.println();
  delay(20);

  sensorValue = analogRead(sensorPin);
  float voltage = sensorValue * (5.0 / 1023.0); //Converts ADC value to voltage 

  //IMPORTANT: The complete String has to be of the Form: 1234,1234,1234,1234; every Value has to be seperated through a comma (',') and the message has to end with a semikolon (';'))
  
  BTserial.print(totalNewtons); // Newtonit
  BTserial.print(",");
  BTserial.print(totalACC); // Kiihtyvyys
  BTserial.print(",");
  BTserial.print("voima");
  BTserial.print(",");
  BTserial.print(voltage); // Voltit
  
  BTserial.print(";"); //message to the receiving device

  if(totalNewtons >= 10 && totalNewtons <=75 )
  {
    BTserial.print("Are u even trying");
    BTserial.print(",");
    
    
  }
  else if(totalNewtons >= 75 && totalNewtons <=150 )
  {
    BTserial.print("U hit like bitch");
    BTserial.print(",");
    
  }
   else if(totalNewtons >= 170 && totalNewtons <=250 )
  {
    BTserial.print("U the Man");
    BTserial.print(",");
    
  }
  else
  {
     BTserial.print("Try to hit");
    BTserial.print(",");
    }



  
  delay(20);
}
 /*int BluetoothSendData()
{
sensorValue = analogRead(sensorPin);
float voltage = sensorValue * (5.0 / 1023.0); // Muutetaan sensorvalue volteiksi

//IMPORTANT: The complete String has to be of the Form: 1234,1234,1234,1234; every Value has to be seperated through a comma (',') and the message has to end with a semikolon (';'))
  
  BTserial.print(totalNewtons); // Newtonit
  BTserial.print(",");
  BTserial.print(totalACC); // Kiihtyvyys
  BTserial.print(",");
  BTserial.print("voima");
  
  //BTserial.print(",");
  //BTserial.print("1234 hPa");
  //BTserial.print(",");
  //BTserial.print("500 ml/s");
  //BTserial.print(",");
  BTserial.print(",");
  BTserial.print(voltage); // Voltit
  
  BTserial.print(";"); //message to the receiving device
  
  delay(20);
} */

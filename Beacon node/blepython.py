from bluepy.btle import Scanner, DefaultDelegate, Peripheral
import threading
import json
import time
import datetime


config = {
	"apiKey": "AIzaSyD9SPtw5ESpiRjaHMIUppTGXBbH-6zz4nY",
	"authDomain": "blebeacon-ao3dd.firebaseapp.com",
	"databaseURL": "https://blebeacon-a03dd.firebaseio.com",
	"projectId": "blebeacon-a03dd",
	"storageBucket": "blebeacon-a03dd.appspot.com",
	"messagingSenderId": "373071844043",
	"appId": "1:373071844043:web:edc4fb32403aeef1d9414e"
}


class NotificationDelegate(DefaultDelegate):

    def __init__(self, number):
        DefaultDelegate.__init__(self)
        self.number = number

    def handleNotification(self, cHandle, data):
        print(data)
        
        data = str(data).split("'")
        (tmp0, tmp1, tmp2) = str(data[1]).split(":")
        
        bname = str(tmp0)
        humid = float(tmp1)
        tempe = float(tmp2)
        
        print(baname)
        print(tempe)
        print(humid)
        
        ts = time.time()
        
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        tmpjson = {"Timestamp": timestamp, "Humidity":humid, "Temperature":tempe}

        jsondata = json.dumps(tmpjson)
        db.child("beacon").child(bname).set(jsondata, user['idToken'])


bt_addrs = ['5c:31:3e:f0:bb:47']
connections = []
connection_threads = []
scanner = Scanner(0)

class ConnectionHandlerThread (threading.Thread):
    def __init__(self, connection_index):
        threading.Thread.__init__(self)
        self.connection_index = connection_index

    def run(self):
        connection = connections[self.connection_index]
        connection.setDelegate(NotificationDelegate(self.connection_index))
        while True:
            try:
                if connection.waitForNotifications(1.0):
                    connection.writeCharacteristic(37, 'Thank you for the notification!')
                    continue
            except:
                pass
                    

while True:
    try:        
        devices = scanner.scan(2)
        for d in devices:    
                p = Peripheral(d)
                connections.append(p)
                t = ConnectionHandlerThread(len(connections)-1)
                t.start()
                connection_threads.append(t)
    except:
        pass

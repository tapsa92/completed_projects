//includes
#include <DHT.h>
#include <LowPower.h>
#include <Adafruit_Sensor.h>

//defines
#define softserial


//constants
#ifdef softserial
  #include <SoftwareSerial.h>
  const int BTRX = 0;  // 1
  const int BTTX = 1;  // 0
  SoftwareSerial SerialBT(BTRX, BTTX);
#else 
  HardwareSerial SerialBT = Serial1;
#endif

//variables
//BT05
#define ble_vcc 9
#define ble_gnd 8

//DHT
#define DHTPIN 7
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

//Global Variables
float hum;
float temp;

int sleep_count;
int i;

const String btname = "Kajak_nano";
//initializing FC-22 SBX
int smokeA0=A5;
int sensorThres = 400;



void setup() {
  //Lets make sure ble is online
  pinMode(ble_vcc, OUTPUT);
  pinMode(ble_gnd, OUTPUT);
  digitalWrite(ble_vcc, HIGH);
  digitalWrite(ble_gnd, LOW);
  
  pinMode(smokeA0,INPUT);
  
  SerialBT.begin(9600);
  Serial.begin(9600);
  dht.begin();
}



void loop() {


  //wake up bluetooth and give time for bluetooth to wake up
  pinMode(ble_gnd, OUTPUT);
  delay(120);
  
  //Loop the temp and humidity readings 5 times
 for(int i=0;i<5;i++){
    hum = dht.readHumidity(); // humidity
    temp = dht.readTemperature(); // celsius temp
    delay(100);
  }

  //sending CO2 to serialmonitor

  int analogSensor=analogRead(smokeA0);

  //BLE message new
  SerialBT.print(temp);
  SerialBT.print(hum);
 // SerialBT.println(analogSensor);
  
  delay(50);

  //Make bluetooth sleep before making arduino sleep
  pinMode(ble_gnd, INPUT);
  //LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
// 4 hours = 60x60x4 = 14400 s
// 14400 s / 8 s = 1800
unsigned int sleepCounter;
for (sleepCounter = 1; sleepCounter > 0; sleepCounter--)
{
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);  
}
Serial.print(temp);
Serial.print(hum);

  
  delay(100);

/*Serial.print("temp: ");
Serial.println(temp);
Serial.print("kosteus: ");
Serial.println(hum);
Serial.print("Smoke: ");
Serial.println(analogSensor);
*/


  
    
}  



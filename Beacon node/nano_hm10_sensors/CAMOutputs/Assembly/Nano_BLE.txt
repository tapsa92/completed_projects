Partlist exported from C:/Users/tapiooht/Documents/EAGLE/projects/NANO_BLE/Nano_BLE.sch at 5.11.2019 9.54

Qty Value                 Device                Package               Parts Description                                             POPULARITY
1                         PINHD-1X6/90          1X06/90               JP1   PIN HEADER                                              10        
1   ARDUINO-NANO-3.0#ISP  ARDUINO-NANO-3.0#ISP  ARDUINO-NANO-3.0#ISP  M1    Arduino Nano 3.0 (and compatible devices) with ISP pads           
1   GAS-SENSOR-MQ2        GAS-SENSOR-MQ2        GAS-SENSOR-MQ2        U$2   Gas Sensor based on MQ-2 device                                   
1   TEMP-HUM-SENSOR-DHT22 TEMP-HUM-SENSOR-DHT22 TEMP-HUM-SENSOR-DHT22 U$3   DHT22 - digital humidity & temperature sensor                     

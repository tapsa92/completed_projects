package com.to.varusmies_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.CheckBox;

public class haavat extends AppCompatActivity {

    //checkboxien lisäys
    CheckBox cb,cb1,cb2,cb3,cb4,cb5,cb6;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // DROPDOWN MENU
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haavat);

        //TOOLBAR
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Back -nappula
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Takaisin aloitusruutuun
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

        cb=(CheckBox) findViewById(R.id.checkBox);
        cb1=(CheckBox) findViewById(R.id.checkBox1);
        cb2=(CheckBox) findViewById(R.id.checkBox2);
        cb3=(CheckBox) findViewById(R.id.checkBox3);
        cb4=(CheckBox) findViewById(R.id.checkBox4);
        cb5=(CheckBox) findViewById(R.id.checkBox5);
        cb6=(CheckBox) findViewById(R.id.checkBox6);

    }

    // siirrytään activityjen välillä
    public void activityyn_siirto_btn(View h)
    {
        if(cb3.isChecked()&& cb5.isChecked()&! cb1.isChecked()&! cb2.isChecked()&! cb3.isChecked() &! cb4.isChecked()&! cb6.isChecked())
        {
            Intent i = new Intent(this, hoitoon_1vrk.class);
            startActivity(i);
        }
        else if(cb2.isChecked()&! cb1.isChecked() &!cb3.isChecked() &! cb4.isChecked()&! cb5.isChecked() &! cb6.isChecked())
        {
            Intent i =new Intent(this, aamusta_hoitoon.class);
            startActivity(i);
        }

        else if (cb5.isChecked() &! cb1.isChecked() &! cb2.isChecked() &! cb3.isChecked() &! cb4.isChecked() &! cb6.isChecked())
        {
            //pitää tehdä tähän sopivampi activity
            Intent i = new Intent(this, odotusilmoitus.class);
            startActivity(i);
        }

        else if (cb3.isChecked() &! cb1.isChecked() &! cb2.isChecked() &! cb5.isChecked() &! cb4.isChecked() &! cb6.isChecked())
        {
            //pitää tehdä tähän sopivampi activity
            Intent i = new Intent(this, hoitoon_1vrk.class);
            startActivity(i);
        }

        else
        {
            Intent i =new Intent(this,paivystykseen.class);
            startActivity(i);
        }
    }

}

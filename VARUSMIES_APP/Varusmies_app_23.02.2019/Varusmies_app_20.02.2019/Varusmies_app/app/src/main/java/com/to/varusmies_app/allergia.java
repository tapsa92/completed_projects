package com.to.varusmies_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class allergia extends AppCompatActivity {

    //checkboxien lisäys
    CheckBox cb,cb1,cb2,cb3,cb4;
    int avuntarve =0;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allergia);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Back -nappula
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Takaisin aloitusruutuun
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

        cb=(CheckBox) findViewById(R.id.checkBox);
        cb1=(CheckBox) findViewById(R.id.checkBox1);
        cb2=(CheckBox) findViewById(R.id.checkBox2);
        cb3=(CheckBox) findViewById(R.id.checkBox3);
        cb4=(CheckBox) findViewById(R.id.checkBox4);


    }
    // siirrytään odotustulokset ilmoittavaan ikkunaan sen jälkeen kun buttonia on painettu

    public void odotus_activityyn(View h)
    {
        if(cb4.isChecked()&! cb.isChecked()&! cb1.isChecked()&! cb2.isChecked()&! cb3.isChecked())
        {

            Intent i=new Intent(this, hoitoon_1vrk.class);

            //lähtetään int arvo odotusilmoitus classiin millä voidaan sitten muokata textviewta ja antaa odotusaika
            i.putExtra("avuntarve_int",1);

            startActivity(i);

                /*
                //Mallina miten edellisessä ohjelmassa tein
                text.setText("Pitäisiköhän vielä varmistaa jotain?");

                //Intent intent= new Intent(this, DisplayMessageActivity.class);
                //startActivity(intent);
                Intent intent = new Intent(this, Valmis2.class);
                startActivity(intent);
                */
        }
        else
        {
            Intent i =new Intent(this,paivystykseen.class);
            startActivity(i);
        }

    }
}

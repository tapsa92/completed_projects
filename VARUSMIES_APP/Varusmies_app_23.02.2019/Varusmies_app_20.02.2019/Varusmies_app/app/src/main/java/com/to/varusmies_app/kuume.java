package com.to.varusmies_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

public class kuume extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // DROPDOWN MENU
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuume);

        //TOOLBAR
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Back -nappula
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Takaisin aloitusruutuun
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });
    }
 // Palataan aloitus activityyn
    public void main_activity(View h)
    {
        Intent intent =new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

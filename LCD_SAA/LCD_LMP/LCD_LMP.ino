// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain
#include <LiquidCrystal.h>
#include "DHT.h"

#define DHTPIN 7     // what pin we're connected to
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
#define DHTTYPE DHT11   // DHT 11
int analogpin = A0;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println("DHT11 test!");

   // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  
  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  int smoke;
  smoke= analogRead(analogpin);
  int h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  int t = dht.readTemperature();
  
 
  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print("%    ");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print("*C   ");
  Serial.print("Smoke: ");
  Serial.print(smoke);
  Serial.print("    time from start(s): ");
  Serial.println(millis() / 1000);

  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 0);
  lcd.print("Hum: ");
  lcd.print(h);

  lcd.setCursor(8, 0);
  lcd.print("MQ2: ");
  lcd.print(smoke);
  
  lcd.setCursor(3,1);
  lcd.print("Temp: ");
  lcd.print(t);
  lcd.print("C");
 
}

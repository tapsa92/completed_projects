/*
 * Tapio Ohtonen & Valtteri Lampinen
 * 19.03.2019
 * Arduino Uno, SHT31...
 * Lämmön ja kosteuden tulostus 
*/


#include <Wire.h>
#include <I2C_LCD.h>

int lmpPin = A0;
int kosteusPin= A1;
int val0;
int val1;

String vali= " ";//välien tekemiseen näytölle

I2C_LCD LCD;
uint8_t I2C_LCD_ADDRESS = 0x51; //Device address configuration, the default value is 0x51.

//For detials of the function useage, please refer to "I2C_LCD User Manual". 
//You can download the "I2C_LCD User Manual" from I2C_LCD WIKI page: http://www.seeedstudio.com/wiki/I2C_LCD

//The usage of LCD.print() is same to the Serial.print().
//For more informations about print, please visit: http://www.arduino.cc/en/Serial/Print

void setup(void)
{
    Wire.begin();         //I2C controller initialization.
    Serial.begin(9600);
    
}

void loop(void)
{
    LCD.CleanAll(WHITE);    //Clean the screen with black or white.
    delay(100);            //Delay for 1s.
    
    //8*16 font size, auto new line, black character on white back ground.
    LCD.FontModeConf(Font_8x16_1, FM_ANL_AAA, BLACK_BAC); 

    //Set the start coordinate.
    LCD.CharGotoXY(0,16);

    while(1)
    {
        float lmp;
        float kosteus;
        float voltageA0;
        float voltageA1;

        val0 = analogRead(lmpPin);
        Serial.println(val0);
        voltageA0=val0*(5.0/1024.0);
        Serial.print("volt lmp ");
        Serial.println(voltageA0); 
        lmp= (voltageA0*28)-40;
        Serial.println(lmp);    
        
        val1 = analogRead(kosteusPin);
        
        Serial.println(val1);
        voltageA1=val1*(5.0/1024.0); //muutetaan raakadata volteiksi
        Serial.print("volt kosteus ");
        Serial.println(voltageA1);
        kosteus = (voltageA1*20);           //muutetaan voltit kosteusprosenteiksi
        
        Serial.println(kosteus);  

        
        //Set the start coordinate. jos ei ole määritettynä tälle funktiolle start kordinaattia se alkaa tulostamaa peräkkäin arvoja. johtuu siitä kun ollaan while funktion sisällä eikä ole määritettynä clear funktiota
        LCD.CharGotoXY(0,0);
        //lmp = analogRead(lmpPin);
        LCD.print("Lmp: ");
        LCD.print(lmp);
        LCD.print("°C");
        //LCD.print(vali);
 
        LCD.CharGotoXY(0,32);
        //kosteus= analogRead(kosteusPin);
        LCD.print("Kosteus: ");
        LCD.print(kosteus);
        LCD.print("%");
        //LCD.print(vali);

        delay(1000);

    }


   /* LCD.print("Seconds:");
    while(1)
    {
        //Set the start coordinate. jos ei ole määritettynä tälle funktiolle start kordinaattia se alkaa tulostamaa peräkkäin arvoja.
        LCD.CharGotoXY(68,32);
        //Print the number of seconds since reset:
        LCD.print(millis()/1000,DEC);
        delay(1000);  
    }*/
    
}


#include <Adafruit_Sensor.h>
#include <SPI.h>
#include <BLEPeripheral.h>
#define BLE_REQ 10
#define BLE_RDY 2
#define BLE_RST 9

//DHT 22 library and things
#include "DHT.h"
#define DHTPIN 7
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

BLEPeripheral blePeripheral = BLEPeripheral();

BLEService Dht22 = BLEService("00001809-0000-1000-8000-00805F9B34FB"); 

BLEDoubleCharacteristic sensors = BLEDoubleCharacteristic("00002A1C-0000-1000-8000-00805F9B34FB",BLERead | BLENotify);
BLEDescriptor sensorValueDescriptor = BLEDescriptor("00002902-0000-1000-8000-00805f9b34fb", "Temperature");

//BLEDoubleCharacteristic humidity = BLEDoubleCharacteristic("00002A2C-0000-1000-8000-00805F9B34FB", BLERead | BLENotify);
//BLEDescriptor humidityValueDescriptor = BLEDescriptor("00002902-0000-1000-8000-00805f9b34fb", "Humidity");

void setup() {
  Serial.begin(9600);
  dht.begin();//sensor begin

  // set advertised local name and service UUID:
  blePeripheral.setLocalName("Dht22");
  blePeripheral.setDeviceName("TEMPPI");

  blePeripheral.setAdvertisedServiceUuid(Dht22.uuid());

  // add service and characteristic:
  blePeripheral.addAttribute(Dht22);//service
  blePeripheral.addAttribute(sensors); // add the sensor characteristic
  blePeripheral.addAttribute(sensorValueDescriptor);

//  blePeripheral.addAttribute(humidity); // add the sensor characteristic
//  blePeripheral.addAttribute(humidityValueDescriptor);

  // begin advertising BLE Light service:
  blePeripheral.begin();
  Serial.println("Dht22 begin!");
}

void loop() {

 // double temp = dht.readTemperature(); //read temperature
  double temp =random(0, 40); //read temperature
  sensors.setValue(temp);//send temperature value
  Serial.print("Temperature: ");
  Serial.println(temp);
  blePeripheral.poll();
  delay(1000);
  //double humi = dht.readHumidity();
  double humi = random(0, 100);
  Serial.print("Humidity: ");
  Serial.println(humi);
  //humidity.setValue(humi);
  blePeripheral.poll();
  delay(1000);
}




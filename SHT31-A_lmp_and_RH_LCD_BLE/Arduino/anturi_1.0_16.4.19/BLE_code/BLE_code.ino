// Import libraries
#include <SPI.h>
#include <BLEPeripheral.h>// käytetty BLE kirjasto
//#include "Adafruit_BLE_UART.h"
#include <Wire.h>
#include <I2C_LCD.h>
#include <movingAvg.h>

// define pins (varies per shield/board)
#define LED_PIN 3
#define BUTTON_PIN 4

//BLE pinnit
#define BLE_REQ 10
#define BLE_RDY 2
#define BLE_RST 9



int currentState;
int debounceState;
int switchState = 0;
int ledState = 0;


// create a peripheral instance using the BLEPeripheral library.
BLEPeripheral blePeripheral = BLEPeripheral(BLE_REQ, BLE_RDY, BLE_RST);

// Create a service with UUID of 0×FF10.  uuid's can be:
//   16-bit: "ffff"
//  128-bit: "19b10010e8f2537e4f6cd104768a1214"


BLEService lmp = BLEService("FF15");

BLECharCharacteristic lmpCharacteristic= BLECharCharacteristic("FF16", BLERead | BLENotify);

BLEDescriptor lmpDescriptor("2901", "lmp");  


void setup() {
Serial.begin(9600);
pinMode(LED_PIN, OUTPUT);
pinMode(BUTTON_PIN, INPUT);

//Set the advertised Local Name and Device Name; see “Device Name Versus Local
//Name” for more details about why we need to set both of these advertised characteristics.
blePeripheral.setLocalName("SHT31_A Tapio");
blePeripheral.setDeviceName("SHT31_A Tapio");

//Set the UUID of the advertised service.
blePeripheral.setAdvertisedServiceUuid(lmp.uuid());

// add the service and associated characteristics and descriptors to the peripherals instance.
blePeripheral.addAttribute(lmp);
blePeripheral.addAttribute(lmpCharacteristic);
blePeripheral.addAttribute(lmpDescriptor);



//Begin advertising the Bluetooth LE service.
blePeripheral.begin();
  Serial.println("Bluetooth device active, waiting for connections...");

//Assign an event handler method to be called when a write command is made on the peripheral.

}
                void loop() {
                
                //Poll of Bluetooth LE messages
                blePeripheral.poll();
   
                //kuunnellaan onko BLE laiteita mihin yhdistää
                BLECentral central = blePeripheral.central();

                    int i=random(0,40);
                    lmpCharacteristic.setValue(i);

                    //Yhdistetään laitteisiin
               
                    delay(50);
                }
         


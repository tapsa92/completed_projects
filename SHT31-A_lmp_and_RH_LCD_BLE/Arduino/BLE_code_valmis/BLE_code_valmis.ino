/*
 * Tapio Ohtonen & Valtteri Lampinen
 * 28.04.2019
 * Arduino Uno, SHT31-A, Sparking I2C_LCD, NRF8001
 * Lämmön ja kosteuden tulostus BLE:en kautta mobiilipäätteeseen
 * 
*/

#include <Adafruit_Sensor.h>
#include <SPI.h>
#include <BLEPeripheral.h>
#include <movingAvg.h>


#define BLE_REQ 10
#define BLE_RDY 2
#define BLE_RST 9

//BLE GATT protokolien alustus
BLEPeripheral blePeripheral = BLEPeripheral();

BLEService Dht22 = BLEService("00001809-0000-1000-8000-00805F9B34FB"); 

BLEService Dht22humidity = BLEService("FF12"); 

BLEDoubleCharacteristic sensors = BLEDoubleCharacteristic("00002A1C-0000-1000-8000-00805F9B34FB",BLERead | BLENotify);
BLEDescriptor sensorValueDescriptor = BLEDescriptor("00002902-0000-1000-8000-00805f9b34fb", "Temperature");

BLEDoubleCharacteristic humidity = BLEDoubleCharacteristic("FF13", BLERead | BLENotify);
BLEDescriptor humidityValueDescriptor = BLEDescriptor("2901", "Humidity");

long previousMillis = 0; // will store last time sensor was read
long interval = 2000; // interval at which to read sensor (milliseconds)
movingAvg avgRH(40);
movingAvg avgtemp(40);

int lmpPin = A0;
int kosteusPin= A1;
int val0;
int val1;

void setup() {

  Serial.begin(9600);
  
  // set advertised local name and service UUID:
  blePeripheral.setLocalName("SHT31_A");
  blePeripheral.setDeviceName("TEMPPI");

  blePeripheral.setAdvertisedServiceUuid(Dht22.uuid());

  // add service and characteristic:
  blePeripheral.addAttribute(Dht22);//service
  blePeripheral.addAttribute(sensors); // add the sensor characteristic
  blePeripheral.addAttribute(sensorValueDescriptor);

  blePeripheral.addAttribute(Dht22humidity);//service
 blePeripheral.addAttribute(humidity); // add the sensor characteristic
 blePeripheral.addAttribute(humidityValueDescriptor);

  // begin advertising BLE Light service:
  blePeripheral.begin();
  Serial.println("SHT31-A begin!");

    avgRH.begin();      //Raakadatasta keskiarvoa
    avgtemp.begin();
}

void loop() {


if(millis() - previousMillis > interval) {
  pollSensors();
previousMillis = millis();
}

  blePeripheral.poll();
 
}

void pollSensors()
{
          val1 = analogRead(kosteusPin);
           val0 = analogRead(lmpPin);
               
            // Laskuja
            float conversionFactor_RH = 5.0/1023;
            float conversionFactor_temp = 5.0/1023;
            int avg_RH_RAW = avgRH.reading(val1);    // calculate the moving average of RAW
            int avg_temp_RAW = avgtemp.reading(val0);    // calculate the moving average of RAW
            float RH_voltage = avg_RH_RAW * conversionFactor_RH;
            float temp_voltage = avg_temp_RAW * conversionFactor_temp;
            
            //int RH_final = (RH_voltage/5)*125-12.5; //VÄÄRÄ KAAVA KAAPILLE MENNESSÄ
            int RH_final = (RH_voltage)*20;
           // float temp_final = (temp_voltage/5)*218.75-66.875; //VÄÄRÄT KAAVA KAAPILLE MENNESSÄ
           float temp_final = (temp_voltage)*28-40;

  double temp =temp_final; //read temperature
  double humi =RH_final;
  sensors.setValue(temp);//send temperature value
  humidity.setValue(humi);
  Serial.print("Temperature: ");
  Serial.println(temp);
  blePeripheral.poll();
  
  Serial.print("Humidity: ");
  Serial.println(humi);
  
}




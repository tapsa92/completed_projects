
/*
 * Tapio Ohtonen & Valtteri Lampinen
 * 19.03.2019
 * Arduino Uno, SHT31-A, Sparking I2C_LCD
 * Lämmön ja kosteuden tulostus LCD:lle
 * 
*/
#include <SPI.h>
#include <Wire.h>
#include <I2C_LCD.h>
#include <BLEPeripheral.h>
#include <movingAvg.h>
// https://github.com/JChristensen/movingAvg

// Connect CLK/MISO/MOSI to hardware SPI
// e.g. On UNO & compatible: CLK = 13, MISO = 12, MOSI = 11
#define BLE_REQ 10
#define BLE_RDY 2
#define BLE_RST 9

int lmpPin = A0;
int kosteusPin= A1;
int val0;
int val1;
// Kuinka monta samplee average laskuihin
movingAvg avgRH(40);
movingAvg avgtemp(40);

I2C_LCD LCD;
uint8_t I2C_LCD_ADDRESS = 0x51; //Device address configuration, the default value is 0x51.

//For detials of the function useage, please refer to "I2C_LCD User Manual". 
//You can download the "I2C_LCD User Manual" from I2C_LCD WIKI page: http://www.seeedstudio.com/wiki/I2C_LCD

//The usage of LCD.print() is same to the Serial.print().
//For more informations about print, please visit: http://www.arduino.cc/en/Serial/Print

void setup(void)
{
    Wire.begin();         //I2C controller initialization.
    while(!Serial); // Leonardo/Micro should wait for serial init
    Serial.println(F("Lämmön ja kosteuden tulostus T & V"));
    Serial.begin(9600);
    
    avgRH.begin();      //Raakadatasta keskiarvoa
    avgtemp.begin();    
}



void loop(void)
{
    LCD.CleanAll(WHITE);    //Clean the screen with black or white.
    delay(100);            //Delay for 1s.
    
    //8*16 font size, auto new line, black character on white back ground.
    LCD.FontModeConf(Font_8x16_1, FM_ANL_AAA, BLACK_BAC); 

    //Set the start coordinate.
    LCD.CharGotoXY(0,16); 

        while(1)
        {
    
           val1 = analogRead(kosteusPin);
           val0 = analogRead(lmpPin);
                 
            // Laskuja
            float conversionFactor_RH = 5.0/1023;
            float conversionFactor_temp = 5.0/1023;
            int avg_RH_RAW = avgRH.reading(val1);    // calculate the moving average of RAW
            int avg_temp_RAW = avgtemp.reading(val0);    // calculate the moving average of RAW
            float RH_voltage = avg_RH_RAW * conversionFactor_RH;
            float temp_voltage = avg_temp_RAW * conversionFactor_temp;
                 
            int RH_final = (RH_voltage)*20;
            float temp_final = (temp_voltage)*28-40;
    
              // Tulostetaan arvot LCD:lle
    
            LCD.CharGotoXY(0,0);
            LCD.print("RH: ");
            LCD.print(RH_final);
            LCD.print(" %");         
            
            LCD.CharGotoXY(0,32);
            LCD.print("Temp: ");
            LCD.print(temp_final);
            LCD.print(" C"); 
    
            String sendlmp=String(temp_final);
              
        }
}


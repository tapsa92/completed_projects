




/*
 * Tapio Ohtonen & Valtteri Lampinen
 * 19.03.2019
 * Arduino Uno, SHT31...
 * Lämmön ja kosteuden tulostus 
 * 
 * BLE koodissa käytetty Adafruitin echoDemo esimerkkiä pohjana. Written by Kevin Townsend/KTOWN  for Adafruit Industries.
*/
#include <SPI.h>
#include "Adafruit_BLE_UART.h"
#include <Wire.h>
#include <I2C_LCD.h>
#include <movingAvg.h>
// https://github.com/JChristensen/movingAvg


// Connect CLK/MISO/MOSI to hardware SPI
// e.g. On UNO & compatible: CLK = 13, MISO = 12, MOSI = 11
#define ADAFRUITBLE_REQ 10
#define ADAFRUITBLE_RDY 2     // This should be an interrupt pin, on Uno thats #2 or #3
#define ADAFRUITBLE_RST 9

Adafruit_BLE_UART BTLEserial = Adafruit_BLE_UART(ADAFRUITBLE_REQ, ADAFRUITBLE_RDY, ADAFRUITBLE_RST);

int lmpPin = A0;
int kosteusPin= A1;
int val0;
int val1;
// Kuinka monta samplee average laskuihin
movingAvg avgRH(40);
movingAvg avgtemp(40);

//String vali= " ";//välien tekemiseen näytölle

I2C_LCD LCD;
uint8_t I2C_LCD_ADDRESS = 0x51; //Device address configuration, the default value is 0x51.

//For detials of the function useage, please refer to "I2C_LCD User Manual". 
//You can download the "I2C_LCD User Manual" from I2C_LCD WIKI page: http://www.seeedstudio.com/wiki/I2C_LCD

//The usage of LCD.print() is same to the Serial.print().
//For more informations about print, please visit: http://www.arduino.cc/en/Serial/Print

void setup(void)
{
    Wire.begin();         //I2C controller initialization.
    while(!Serial); // Leonardo/Micro should wait for serial init
    Serial.println(F("Lämmön ja kosteuden tulostus T & V"));
    Serial.begin(9600);
    
    avgRH.begin();      //Raakadatasta keskiarvoa
    avgtemp.begin();

    BTLEserial.setDeviceName("BLE T&V"); /* 7 characters max! */

    BTLEserial.begin();
    
}

aci_evt_opcode_t laststatus = ACI_EVT_DISCONNECTED; //tarkastaa nRF8001 statusta kokoajan

void loop(void)
{
    LCD.CleanAll(WHITE);    //Clean the screen with black or white.
    delay(100);            //Delay for 1s.
    
    //8*16 font size, auto new line, black character on white back ground.
    LCD.FontModeConf(Font_8x16_1, FM_ANL_AAA, BLACK_BAC); 

    //Set the start coordinate.
    LCD.CharGotoXY(0,16);

    int a=0; 

        while(1)
        {
    
           val1 = analogRead(kosteusPin);
           val0 = analogRead(lmpPin);
     
           // val1 = constrain(val1, 102, 921);   // Constrainttia anturien analog -arvoihin
           // val0 = constrain(val0, 125, 896);
            //RH = map(val1, 102, 921, 0, 100);
            //unsigned short RH_scaled = map(val1, 102, 921, 0, 1023);
            //unsigned short temp_scaled = map(val0, 125, 896, 0, 1023);
    
            // Laskuja
            float conversionFactor_RH = 5.0/1023;
            float conversionFactor_temp = 5.0/1023;
            int avg_RH_RAW = avgRH.reading(val1);    // calculate the moving average of RAW
            int avg_temp_RAW = avgtemp.reading(val0);    // calculate the moving average of RAW
            float RH_voltage = avg_RH_RAW * conversionFactor_RH;
            float temp_voltage = avg_temp_RAW * conversionFactor_temp;
            
            //int RH_final = (RH_voltage/5)*125-12.5; //VÄÄRÄ KAAVA KAAPILLE MENNESSÄ
            int RH_final = (RH_voltage)*20;
           // float temp_final = (temp_voltage/5)*218.75-66.875; //VÄÄRÄT KAAVA KAAPILLE MENNESSÄ
           float temp_final = (temp_voltage)*28-40;
    
           //Tulostetaan serialmonitoriin voltaget ja muunnetut arvot
           /*
            Serial.println("");
            Serial.print("RH_raaka: ");
            Serial.println(kosteusPin);
            Serial.print("RH_volt: ");
            Serial.println(RH_voltage);
            Serial.print("RH: ");
            Serial.println(RH_final);
            
            Serial.println("");
            Serial.print("TEMP_raaka: ");
            Serial.println(lmpPin);        
            Serial.print("TEMP_volt: ");
            Serial.println(temp_voltage);  
            Serial.print("TEMP: ");
            Serial.println(temp_final);  
            */
            
            a++;

              // Tulostetaan arvot LCD näytölle
    
            LCD.CharGotoXY(0,0);
            LCD.print("RH: ");
            LCD.print(RH_final);
            LCD.print(" %");
            
            
            LCD.CharGotoXY(0,32);
            LCD.print("Temp: ");
            LCD.print(temp_final);
            LCD.print(" C"); 
    
            delay(10);
            // Sekunnin välein näyttöön print, oikeasti samplee 1ms välein ja tulostaa keskiarvon
           // if(a==200){
            // SARJALIIKENNE HAJOAA, JOS SAMAAAN AIKAAN PRINTTAA MONITORIIN JA NÄYTTÖÖN

            //a=0;

              
              String sendlmp=String(temp_final);
              

             




            
           // }

                                 //nRF8001 functions start here
                                 // Tell the nRF8001 to do whatever it should be working on.
                        BTLEserial.pollACI();
                      
                        // Ask what is our current status
                        aci_evt_opcode_t status = BTLEserial.getState();
                        
                        // If the status changed....
                        if (status != laststatus)
                          {
                            // print it out!
                            if (status == ACI_EVT_DEVICE_STARTED) {
                                Serial.println(F("* Advertising started"));
                            }
                            if (status == ACI_EVT_CONNECTED) {
                                Serial.println(F("* Connected!"));
                            }
                            if (status == ACI_EVT_DISCONNECTED) {
                                Serial.println(F("* Disconnected or advertising timed out"));
                            }
                            // OK set the last status change to this one
                            laststatus = status;
                          }
                      
                        if (status == ACI_EVT_CONNECTED) 
                          {
                            // Lets see if there's any data for us!
                            if (BTLEserial.available()) 
                            {
                              Serial.print("* "); Serial.print(BTLEserial.available()); Serial.println(F(" bytes available from BTLE"));
                            }
                            // OK while we still have something to read, get a character and print it out
                            while (BTLEserial.available()) 
                            {
                              char c = BTLEserial.read();
                              Serial.print(c);
                            }
                        
                            // Next up, see if we have any data to get from the Serial console
                        
                                  if (Serial.available())
                                  {
                                    // Read a line from Serial
                                    Serial.setTimeout(100); // 100 millisecond timeout
                                    String s = Serial.readString();
                                    
                                    s = String(temp_final); //MUUTETAAN OMAT ARVOT STRINGEIKS TAI TOISINPÄIN JA SAADAA LÄHETTYÄ SE ARVO SERIALMONITORIN KAUTTA PUHELIMEEN
      
       
                              
                                    // We need to convert the line to bytes, no more than 20 at this time
                                    uint8_t sendbuffer[20];
                                    s.getBytes(sendbuffer, 20);
                                    char sendbuffersize = min(20, s.length());
                                    
                              
                                    Serial.print(F("\n* Sending -> \"")); Serial.print((char *)sendbuffer); Serial.println("\"");
                              
                                    
                              
                                    // write the data
                                    BTLEserial.write(sendbuffer, sendbuffersize);
                                    
                                    
                                  }
                            
                          }
         }

}
